package src.polymorphism;

public class Program {
    public static void main(String[] args) {
        Bike a = new Bike();
        Bike b = new Splendor();

        a.run();
        b.run();
    }    
}