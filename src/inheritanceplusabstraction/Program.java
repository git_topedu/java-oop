package src.inheritanceplusabstraction;

public class Program {
    public static void main(String[] args) {
        Cow cow = new Cow("Betty Sue");
        Snake snake = new Snake("Adelle");

        System.out.println(cow.name + " says: " + cow.MakeASound());
        System.out.println(snake.name + " says: " + snake.MakeASound());
    }
}