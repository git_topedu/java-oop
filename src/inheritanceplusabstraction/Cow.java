package src.inheritanceplusabstraction;

public class Cow extends Animal {

    public Cow(String name) {
        this.name = name;
    }
    @Override
    public String MakeASound() {
        return "Moo";
    }

}