package src.inheritanceplusabstraction;

public abstract class Animal {
    public String name;

    public abstract String MakeASound();
}