package src.inheritanceplusabstraction;

public class Snake extends Animal{
    public Snake(String name) {
        this.name = name;
    }

    @Override
    public String MakeASound() {
        return "Hiss";
    }
}