package src.encapsulation;

public class Program {
    public static void main(String[] args) {
        Student student = new Student();

        student.setName("Bob");

        System.out.println("Student's name is: " + student.getName());
    }
}